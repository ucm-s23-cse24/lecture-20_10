#ifndef TICTACTOE_H
#define TICTACTOE_H

#include "board.h"
#include <iostream>


struct Vec{
    int x;
    int y;

    Vec(int x, int y){
        this->x = x;
        this->y = y;
    }
};

struct TicTacToe{
    Board board;

    bool player1;
    bool done;

    Vec (*player2func)(Board);

    TicTacToe(){
        player1 = true;
        done = false;
        board = Board();
    }

    TicTacToe(int size){
        player1 = true;
        done = false;
        board = Board(size);
    }

    void setPlayer2(Vec (*f)(Board)){
        player2func = f;
    }

    void play(){
        while (!done) {
            system("clear");
            std::cout << board;
            if (player1) {
                bool success = false;
                while (!success) {
                    int x, y;
                    std::cout << "Player 1: ";
                    std::cin >> x >> y;
                    success = board.updateGrid(x, y, PLAYER1);
                }
            } else {
                // bool success = false;
                // while (!success) {
                //     int x, y;
                //     std::cout << "Player 2: ";
                //     std::cin >> x >> y;
                //     success = board.updateGrid(x, y, PLAYER2);
                // }

                Vec move = player2func(board);
                bool success = board.updateGrid(move.x, move.y, PLAYER2);
                if (!success){
                    system("clear");
                    std::cout << "Player 1 wins" << std::endl;
                    break;
                }
            }
            player1 = !player1;

            if (board.isWinner(PLAYER1)) {
                system("clear");
                std::cout << "Player 1 wins" << std::endl;
                std::cout << board;
                done = true;
            }
            else if (board.isWinner(PLAYER2)) {
                system("clear");
                std::cout << "Player 2 wins" << std::endl;
                std::cout << board;
                done = true;
            }
            else if (board.isFull()){
                system("clear");
                std::cout << "It's a tie" << std::endl;
                std::cout << board;
                done = true;
            }
        }
    }
};

#endif