#include <iostream>
#include "board.h"
#include "tictactoe.h"

using namespace std;


Vec simple(Board board){
    for (int i = 0; i < board.getSize(); i++){
        for (int j = 0; j < board.getSize(); j++){
            if (board.getPos(i, j) == EMPTY){
                return Vec(i, j);
            }
        }
    }
    return Vec(0, 0);
}

int main() {
    
    TicTacToe myGame;

    myGame.setPlayer2(simple);

    myGame.play();

    return 0;
}